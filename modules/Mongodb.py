#!/usr/bin/env python
"""
Class Mongodb
:author: @Julio Sanchez
:version: 1.0
:contact: julio.sanchez@osom.com
"""
from pymongo import MongoClient
from modules.Config import *


class Mongodb:
    'Config Mongodb client'

    @staticmethod
    def getConnection(config='MONGO'):
        'return connection mongodb'
        parameters = Config.ConfigSectionMap(config)
        host = parameters.host.replace('"', '')
        user = parameters.user.replace('"', '')
        passwd = parameters.passwd.replace('"', '')
        port = parameters.port.replace('"', '')
        path = "mongodb://"
        if (user): path += user + ":" + passwd + "@"
        path = host
        connection = MongoClient(path, int(port))
        return connection
