#!/usr/bin/env python
"""
Class Submit
:author: @Julio Sanchez
:version: 1.0
:contact: julio.sanchez@osom.com
"""
import urllib2
import httplib
import json
from modules.Config import *


class Submit:
    'Url lib requests'

    def __init__(self, type='DEV'):
        'Declare variable'
        parameters = Config().ConfigSectionMap(type)
        self.path = parameters.url.replace('"', '')
        self.user = parameters.user.replace('"', '')
        self.passwd = parameters.passwd.replace('"', '')
        self.charset = parameters.charset.replace('"', '')
        self.port = parameters.port.replace('"', '')
        self.response = ''
        self.error = ''
        self.code = ''

    def Request(self, params, url=''):
        'Send request by urllib2 and json'
        try:
            req = urllib2.Request(self.path + url,
                                  headers={
                                      # "Authorization": basic_authorization(self.user, self.passwd),
                                      "Content-Type": "application/json",
                                      "Accept": "*/*"
                                  }, data=params)

            response = urllib2.urlopen(req)
            self.response = json.load(response)
            self.code = self.response['error']
            return True
        except urllib2.HTTPError, error:
            self.error = 'HTTPError:', str(error.reason)
            return False
        except urllib2.URLError, error:
            self.error = 'URLError:', str(error.reason)
            return False
        except httplib.HTTPException, error:
            self.error = 'HTTPException:', str(error.reason)
            return False
        except Exception, error:
            self.error = "Unexpected error:", str(error.message)
            return False

    def getRespose(self):
        'get Response'
        return self.response

    def getError(self):
        'get error string reason'
        return self.error

    def getCode(self):
        'get code request'
        return self.code
