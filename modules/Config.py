#!/usr/bin/env python
"""
Class Mongodb
:author: @Julio Sanchez
:version: 1.0
:contact: julio.sanchez@osom.com
"""
import ConfigParser
import os


class Config:
    'config parameters.ini'

    @staticmethod
    def ConfigSectionMap(section):
        'Mapping sections by configuration'
        config = ConfigParser.ConfigParser(dict_type=AttrDict)
        path = os.path.dirname(os.path.abspath(__file__))
        config.read(path + '/config/parameters.ini')
        return config._sections[section]


class AttrDict(dict):
    """
    """
    def __init__(self, *args, **kwargs):
        """

        Args:
            args:
            kwargs:
        """
        super(AttrDict, self).__init__(*args, **kwargs)
        self.__dict__ = self
