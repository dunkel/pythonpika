#!/usr/bin/env python
"""
Class Worker
:author: @Julio Sanchez
:version: 1.0
:contact: julio.sanchez@osom.com
"""
import pika
from modules.Config import *


class ConnectionPika:
    """
    """
    def __init__(self):
        """
        Coneccion pika to rabbitmq
        """
        parameters = Config.ConfigSectionMap('RABBITMQ')
        self.host = parameters.host.replace('"', '')
        self.user = parameters.user.replace('"', '')
        self.passwd = parameters.passwd.replace('"', '')
        self.port = parameters.port.replace('"', '')
        self.virtualHost = parameters.virtualhost.replace('"', '')
        credentials = pika.PlainCredentials(str(self.user), str(self.passwd))
        parameters = pika.ConnectionParameters(str(self.host), int(self.port), self.virtualHost, credentials)
        self.connection = pika.BlockingConnection(parameters)
        self.channel = self.connection.channel()

    def getChannel(self):
        """

        Returns:

        """
        return self.channel

    def closeConection(self):
        """

        """
        self.connection.close()
