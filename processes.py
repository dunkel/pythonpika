#!/usr/bin/env python
"""
Class Process
:author: @Julio Sanchez
:version: 1.0
:contact: julio.sanchez@osom.com
"""
import sys
import datetime
import time
from modules.Submit import *
from modules.Mongodb import *
from bson.objectid import ObjectId
from modules.ConnectionPika import *


class Processes:
    """
    Processing requests from rabbitmq
    """

    def __init__(self, queue):
        """
        :param queue: 
        """
        'Declarar variables y coneccion'
        self.clientMongo = Mongodb.getConnection()
        self.queue = queue
        self.error = ""
        self.channel = ConnectionPika().getChannel()

    def Result(self, ch, method, properties, body):
        'Processing callback request'
        data = json.loads(body)
        
        send = Submit();
        result = send.Request(json.dumps(data).encode('utf8'), 'creditmemo/webservice/post')
        response = ''
        error = ''
        code = ''
        if (result):
            response = send.getRespose()
            code = send.getCode()
            if (code == 203 or code == 202):
                error = response['msg']
        else:
            error = send.getError()
        collection = self.clientMongo.creditmemo.log_send_memo
        if (data.has_key("_id")):
            post_id = data['_id']
            log = collection.find_one({"_id": ObjectId(post_id)})
            send = log['count_send'] + 1
            collection.update({'_id': ObjectId(post_id)},
                              {'$set': {
                                  "response": response,
                                  "count_send": send,
                                  "code": code,
                                  "updatedAd": datetime.datetime.utcnow()
                              }});
        else:
            send = 1
            element = {
                "request": data,
                "response": response,
                "error": error,
                "code": code,
                "count_send": send,
                "createdAd": datetime.datetime.utcnow(),
                "updatedAd": datetime.datetime.utcnow()
            }
            post_id = collection.insert_one(element).inserted_id
        if (error != '' and send < 10):
            data['_id'] = str(post_id)
            body = json.dumps(data)
            param = "python /CreditMemo/resubmit.py " + str(self.queue) + " '" + str(body) + "'"
            param.strip()
            param.lstrip()
            param.rstrip()
            os.system(param)

    def QueryRmq(self):
        """
        :return: 
        """
        'Rabbitmq request queue'
        self.channel.queue_declare(queue=self.queue, durable=True)
        self.channel.confirm_delivery()
        self.channel.basic_consume(self.Result,
                                   queue=self.queue,
                                   no_ack=True)
        self.channel.start_consuming()

    def CloseChanel(self):
        """
        :return: 
        """
        'close connection rabbitmq'
        ConnectionPika().closeConection()


'Validate that a parameter'
try:
    queue = sys.argv[1]
    procces = Processes(queue)
    procces.QueryRmq()
except Exception as err:
    print err
    print "We received the name of the queue, Queue name missing"
    print err.message
