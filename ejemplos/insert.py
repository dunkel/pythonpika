#!/usr/bin/env python
import datetime
from pymongo import MongoClient


datos = {
    "ordernr": 208228566,
    "credit_memo": 98140,
    "price": 845.00,
    "free_shipping": "false",
    "send_email": "false",
    "active_voucher": "false",
    "status": {
        "41": ["TO094AT07CCEDFMX-659664"]
    }
}

element = {
    "request": datos,
    "response": "",
    "error": "",
    "createdAd": datetime.datetime.utcnow(),
    "updatedAd" : ""
}


clientMongo = MongoClient('localhost', 27017)

collection = clientMongo.creditmemo.log_send_memo

post_id = collection.insert_one(element).inserted_id

print post_id