#!/usr/bin/env python
import pika
import uuid

class FibonacciClient(object):
    def __init__(self):
        self.connection = pika.AsyncoreConnection(pika.ConnectionParameters(
                host='localhost'))

        self.channel = self.connection.channel()

        result = self.channel.queue_declare(exclusive=True)
        self.callback_queue = result.queue

        self.corr_id = None
        self.channel.basic_consume(self.on_response, no_ack=True,
                                   queue=self.callback_queue)

    def on_response(self, ch, method, props, body):
        if props.correlation_id == self.corr_id:
            self.response = body

    def call(self, n):
        self.corr_id = str(uuid.uuid4())
        self.response = None
        self.channel.basic_publish(exchange='',
                                   routing_key='rpc_queue',
                                   properties=pika.BasicProperties(
                                         reply_to = self.callback_queue,
                                         correlation_id = self.corr_id,
                                         ),
                                   body=str(n))
        while self.response is None:
            pika.asyncore_loop(count=1)
        return self.response

fibonacci_rpc = FibonacciClient()

print " [x] Requesting fib(30)"
response = fibonacci_rpc.call(30)
print " [.] Got %r" % (response,)