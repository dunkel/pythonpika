#!/usr/bin/env python
import pika

connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()

channel.queue_declare(queue='hello')

datos = '{"ordernr": 208228566,"credit_memo": 98140,"price": 845.00,"free_shipping": "false","send_email": "false","active_voucher": "false","status": {"41": ["TO094AT07CCEDFMX-659664"]}}'

channel.basic_publish(exchange='',
                      routing_key='hello',
                      body=datos)
print(" [x] Sent: ", datos)

connection.close()
