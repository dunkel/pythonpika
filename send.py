#!/usr/bin/env python
from modules.ConnectionPika import *

ConnectionPika().closeConection()

channel = ConnectionPika().getChannel()
exchangeName = 'nsEntities'
channel.exchange_declare(exchangeName, 'direct')

entityType = 'credit'
channel.queue_declare(entityType, durable=True, exclusive=False, auto_delete=False)

channel.queue_bind(entityType, exchangeName)

if channel.basic_publish(exchangeName, entityType, body='julio sanchez5',
                         properties=pika.BasicProperties(content_type='application/json')):
    print 'Message was published'
else:
    print 'Message was not published'

ConnectionPika().closeConection()
