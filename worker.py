#!/usr/bin/env python
"""
Class Worker
:author: @Julio Sanchez
:version: 1.0
:contact: julio.sanchez@osom.com
"""
import sys

from modules.ConnectionPika import *
from modules.Mongodb import *


class Worker:
    """
    """
    def __init__(self, queue):
        'Declare variable'
        self.queue = queue
        self.channel = ConnectionPika().getChannel()
        self.clientMongo = Mongodb().getConnection()

    def QueryRmq(self):
        'Rabbitmq request queue'
        self.channel.basic_qos(prefetch_count=1)
        self.channel.queue_declare(queue=self.queue, durable=True)
        self.channel.basic_consume(self.List,
                                   queue=self.queue)
        self.channel.start_consuming()

    def List(self, ch, method, properties, body):
        'List the processes that could not be sent'
        delay = properties.delivery_mode
        # time.sleep(delay)
        print delay
        print body

        new_dalay = delay + 2
        NewProccess(self.queue).InsertRmq(body, new_dalay)
        # self.channel.basic_ack(delivery_tag=method.delivery_tag)
        # data = json.loads(body)
        # send = Submit();
        # result = send.Request(json.dumps(data).encode('utf8'), 'creditmemo/webservice/post')
        # response = ''
        # error = ''
        # code = ''
        # if (result):
        #     response = send.getRespose()
        #     code = send.getCode()
        #     if (code == 203 or code == 202):
        #         error = response['msg']
        # else:
        #     error = send.getError()
        # if (error != ''):
        #     print error
        #     new_dalay = delay*20
        #     NewProccess(self.queue).InsertRmq(body, new_dalay)
        # element = {
        #     "request": data,
        #     "response": response,
        #     "error": error,
        #     "code": code,
        #     "createdAd": datetime.datetime.utcnow(),
        #     "updatedAd": datetime.datetime.utcnow()
        # }
        # collection = self.clientMongo.creditmemo.log_send_memo
        # post_id = collection.insert_one(element).inserted_id
        # print post_id

    def ReturnQueue(self, body, delay):
        """

        Args:
            body:
            delay:
        """
        print body, delay

class NewProccess:
    'Processing new queue from rabbitmq'

    def __init__(self, queue):
        'Declarar variables y coneccion'
        self.queue = queue
        self.channel2 = ConnectionPika().getChannel()

    def InsertRmq(self, data, delay):
        """

        Args:
            data:
            delay:
        """
        print data, delay
        self.channel2.queue_declare(queue=self.queue, durable=True)
        self.channel2.basic_publish(exchange='',
                                    routing_key=self.queue,
                                    body=data,
                                    properties=pika.BasicProperties(
                                        delivery_mode=delay,  # make message persistent
                                    ))

queue = sys.argv[1]
worker = Worker(str('old_') + queue)
worker.QueryRmq()
