#!/usr/bin/env python
"""
Class Process
:author: @Julio Sanchez
:version: 1.0
:contact: julio.sanchez@osom.com
"""
import sys
from modules.ConnectionPika import *


class QueueDelay:
    """
    """
    def __init__(self):
        """

        """
        self.delay_channel = ConnectionPika().getChannel()
        self.channel = ConnectionPika().getChannel()

    def procces(self, entityType, body):
        """
        Args:
            queue (str): 
            body (str): 

        :return:
        """
        exchangeName = 'nsEntities'
        self.channel.exchange_declare(exchangeName, 'direct')
        self.channel.confirm_delivery()
        self.channel.queue_declare(entityType, durable=True, exclusive=False, auto_delete=False)
        self.channel.queue_bind(entityType, exchangeName)
        delayEntityType = entityType + str('_delay')
        self.delay_channel.confirm_delivery()
        self.delay_channel.queue_declare(delayEntityType, durable=True, arguments={
            'x-message-ttl': int(50000),
            'x-dead-letter-exchange': exchangeName,
            'x-dead-letter-routing-key': entityType
        });
        self.delay_channel.queue_bind(delayEntityType, exchangeName)
        if self.delay_channel.basic_publish(exchangeName,
                                            delayEntityType,
                                            body=body,
                                            properties=pika.BasicProperties(content_type='application/json',
                                                                            delivery_mode=2)):
            print 'Message was published'
        else:
            print 'Message was not published'


queue = sys.argv[1]
body = sys.argv[2]

queueDelay = QueueDelay()
queueDelay.procces(queue, body)
